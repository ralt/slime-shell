(require 'slime-repl)

(defun slime-repl-return (&optional end-of-input)
  "Override the slime-repl-return method to hook our special '%' prefix in there."
  (interactive "P")
  (slime-check-connected)
  (cond (end-of-input
         (slime-repl-send-input))
        (slime-repl-read-mode ; bad style?
         (slime-repl-send-input t))

        ;; Override starts here.
        ((ignore-errors
           (string= (buffer-substring
                     slime-repl-input-start-mark
                     (1+ slime-repl-input-start-mark))
                    "%"))
         (progn
           (insert "\n")
           (slime-shell)
           (goto-char (point-max))
           (slime-mark-input-start)
           (slime-mark-output-start)
           (with-current-buffer (slime-output-buffer)
             (slime-repl-insert-prompt)
             (slime-repl-show-maximum-output))))
        ;; Override ends here.
        
        ((and (get-text-property (point) 'slime-repl-old-input)
              (< (point) slime-repl-input-start-mark))
         (slime-repl-grab-old-input end-of-input)
         (slime-repl-recenter-if-needed))
        ((run-hook-with-args-until-success 'slime-repl-return-hooks end-of-input))
        ((slime-input-complete-p slime-repl-input-start-mark (point-max))
         (slime-repl-send-input t))
        (t
         (slime-repl-newline-and-indent)
         (message "[input not complete]"))))

(defvar slime-shell-prompt-format (lambda ()
                                    (format "%s:%s> "
                                            (slime-lisp-package-prompt-string)
                                            (slime-eval `(swank:default-directory)))))

(defvar slime-shell-prompt-properties '(face slime-repl-prompt-face
                                             read-only t slime-repl-prompt t
                                             rear-nonsticky t front-sticky (read-only)
                                             inhibit-line-move-field-capture t
                                             field output))

(defun slime-repl-insert-prompt ()
  "Override slime-repl-insert-prompt to be able to customize the format string."
  (goto-char slime-repl-input-start-mark)
  (unless slime-repl-suppress-prompt
    (slime-save-marker slime-output-start
      (slime-save-marker slime-output-end
        (unless (bolp) (insert-before-markers "\n"))
        (let ((prompt-start (point))

              ;; Override starts here.
              (prompt (funcall slime-shell-prompt-format)))
              ;; Override ends here.

          (slime-propertize-region

              ;; Override starts here.
              slime-shell-prompt-properties
            ;; Override ends here.

            (insert-before-markers prompt))
          (set-marker slime-repl-prompt-start-mark prompt-start)
          (setq buffer-undo-list nil)
          prompt-start)))))

(defun slime-shell-cd (directory)
  (cd directory)
  (slime-cd directory))

(defun slime-shell ()
  "Roughly parse a shell command line, implementing some builtins
  such as `cd', and sending the rest to bash."
  (let* ((command (buffer-substring (1+ slime-repl-input-start-mark) (point)))
         (parts (split-string command)))
    (if (string= (first parts) "cd")
        (if (= (length parts) 1)
            (slime-shell-cd (getenv "HOME"))
          (slime-shell-cd (string-join (rest parts))))
      (with-current-buffer (slime-output-buffer)
        (call-process-shell-command command nil (current-buffer))))))

(provide 'slime-shell)
